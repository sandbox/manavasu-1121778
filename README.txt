Simple Gallery creates a simple gallery using taxonomy and CCK imagefields.
Requires ImageCache to display thumbnails.
Utilizes Lightbox2 to display all images in an album.

REQUIREMENTS
------------
Simple Gallery is dependent on quite a few modules for many
of its features. ImageField, ImageCache and Lightbox2 are the primary
modules that must be previously installed. All of the dependencies
for these modules must also be installed already.

Be sure that to also enable the Imagecache UI as well as an ImageAPI
in order for the images to be displayed properly. 

INSTALLATION
------------
1. Copy the simplegallery folder to the modules directory.
2. At admin/build/modules enable the simplegallery module.
3. Enable permissions at admin/user/permissions.
4. Configure the module at admin/settings/simplegallery.

SETUP AND CONFIGURATION
------------
1. Create a CCK content type to store images as nodes using ImageField.
2. Create a vocabulary and assign it to the content type you created.
3. Create a preset with ImageCache to define image thumbnails.
4. Add terms as albums to your vocabulary.
5. Create image nodes and assign them to album terms.
6. Configure Simple Gallery based on what you did above.
7. Access the gallery by visiting the yoursite.com/gallery.